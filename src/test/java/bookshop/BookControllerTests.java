package bookshop;

import bookshop.Controller.BookController;
import bookshop.Model.Book;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BookControllerTests {
    @Test
    public void should_return_book_name()
    {
        BookController bookController = new BookController();
        Book result = bookController.getTitle();
        assertEquals("name: Praveen, Sree Thanu", result.getName());
    }
}
