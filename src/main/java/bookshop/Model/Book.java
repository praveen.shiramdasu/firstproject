package bookshop.Model;

public class Book {
    private final String name;

    public Book(String title){
        this.name = title;
    }

    public String getName() {
        return name;
    }
}
