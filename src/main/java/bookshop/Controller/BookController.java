package bookshop.Controller;

import bookshop.Model.Book;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class BookController {

    @GetMapping("/")
    public Book getTitle() {
        return new Book("name: Praveen, Sree Thanu");
    }
}
