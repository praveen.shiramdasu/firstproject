FROM openjdk:8-alpine
ADD /build/libs/SpringBootApp-1.0-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]